import React from 'react';
import { Page, Navbar, Block, BlockTitle } from 'framework7-react';

const AboutPage = () => (
  <Page>
    <Navbar title="About" backLink="Back" />
    <BlockTitle>About My App</BlockTitle>
    <Block strong>
      <p>This application is a simple representation of the UI components Provided by Framework7</p>
      <p>   -  Designed by Rasindu Dilshan Dayasena</p>
    </Block>
  
  </Page>
);

  export default AboutPage;
