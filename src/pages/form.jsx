import React from 'react';
import {
  f7,
  Page,
  Navbar,
  List,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Range,
  Block
} from 'framework7-react';


const FormPage = () => {
  const country = 'India SriLanka Malasia SouthAfrica'.split(' ');

  const f7params = {
    name: 'simpleFramework7App', // App name
      theme: 'auto', // Automatic theme detection 
  };

  const openAlert = () => {
    const app = f7;
    app.dialog.confirm("Are you sure you want to register!!");
  };

  const autoComplete= () =>{
    const app = f7;
    var query="s";
    var results = [];
      if (query.length === 0) {
        render(results);
        return;
      }

      for (var i = 0; i < country.length; i++) {
        if (country[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) results.push(country[i]);
      }
      
      app.dialog.alert(results);
  }

  
  
  return (
  <Page name="form">
    <Navbar title="Form" backLink="Back"></Navbar>

    <BlockTitle>Please Fill the form to Register</BlockTitle>
    <List noHairlinesMd>
      <ListInput
        label="Name"
        type="text"
        placeholder="Your name"
      ></ListInput>

      <ListInput
        label="E-mail"
        type="email"
        placeholder="E-mail"
      ></ListInput>

      <ListInput
        label="Password"
        type="password"
        placeholder="Password"
      ></ListInput>

      <ListInput
        label="Gender"
        type="select"
        >
        <option>Male</option>
        <option>Female</option>
      </ListInput>

      <ListInput
        label="Birth date"
        type="date"
        placeholder="Birth day"
        defaultValue="2014-04-30"
      ></ListInput>
      
      <ListItem title="Membership" smartSelect smartSelectParams={{ openIn: 'sheet' }}>
        <select name="mac-windows" defaultValue="gold">
          <option value="gold">Gold</option>
          <option value="platinum">Platinum</option>
          <option value="silver">Silver</option>
        </select>
      </ListItem>

      <div class="list no-hairlines-md">
        <ul>
          <li class="item-content item-input inline-label">
            <div class="item-inner">
              <div class="item-title item-label">Country</div>
              <div class="item-input-wrap">
                <input id="autocomplete-dropdown" type="text" placeholder="Select your country" onChange={autoComplete} />
              </div>
            </div>
          </li>
        </ul>
      </div>

      <span>Activate Mail service</span>
          <label class="toggle toggle-init color-blue">
            <input type="checkbox" checked />
            <span class="toggle-icon"></span>
          </label>

    </List>

    <Block strong>
      <Row tag="p">
        <Button className="col button button-fill open-confirm" large fill raised color="green" onClick={openAlert}>Register</Button>
      </Row>
    </Block>
  </Page>
);
};

export default FormPage;
