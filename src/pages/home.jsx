import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Swiper,
  SwiperSlide,
  Button
} from 'framework7-react';
import image_1 from '../images/image1.png';
import image_2 from '../images/image2.png';
import image_3 from '../images/image3.png';
import  "../css/app.css";


const HomePage = () => (
  <Page name="home">
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavLeft>
        <Link iconIos="f7:menu" iconAurora="f7:menu" iconMd="material:menu" panelOpen="left" />
      </NavLeft>
      <NavTitle sliding>simpleFramework7App</NavTitle>
      <NavTitleLarge>simpleFramework7App</NavTitleLarge>
    </Navbar>
    {/* Toolbar */}
    {/* <Toolbar bottom>
      <Link>Left Link</Link>
      <Link>Right Link</Link>
    </Toolbar> */}
    {/* Page content */}
    {/* <Block strong>
      <p>Here is your blank Framework7 app. Let's see what we have here.</p>
    </Block> */}

<Swiper pagination navigation scrollbar>
  <SwiperSlide><img class="center" src={image_1} width="50%" height="60%" alt="" /> </SwiperSlide>
  <SwiperSlide><img class="center" src={image_2} width="50%" height="60%" alt="" /></SwiperSlide>
  <SwiperSlide><img class="center" src={image_3} width="50%" height="60%" alt="" /></SwiperSlide>
</Swiper>


    <BlockTitle>Navigation</BlockTitle>
    <List>
      <ListItem link="/about/" title="About"/>
      <ListItem link="/form/" title="Form"/>
      <ListItem link="/message/" title="Messages"/>
    </List>


  </Page>

);
export default HomePage;